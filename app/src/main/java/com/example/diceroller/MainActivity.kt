package com.example.diceroller

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView

/**
 * This activity allows the user to roll two dices and view the result on the screen.
 */
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val rollButton: Button = findViewById(R.id.button)
        rollButton.setOnClickListener {
            rollDice()
        }
    }

    /**
     * Rolls the Dices and updates the screen with the result.
     */
    private fun rollDice() {
        val dice = Dice(6)

        //A random number is generated and the dice on the LEFT are changed according to this number.
        val diceRollLeft = dice.roll()
        val diceImageLeft: ImageView = findViewById(R.id.imageViewLeft)
        when (diceRollLeft) {
            1 -> diceImageLeft.setImageResource(R.drawable.dice_1)
            2 -> diceImageLeft.setImageResource(R.drawable.dice_2)
            3 -> diceImageLeft.setImageResource(R.drawable.dice_3)
            4 -> diceImageLeft.setImageResource(R.drawable.dice_4)
            5 -> diceImageLeft.setImageResource(R.drawable.dice_5)
            6 -> diceImageLeft.setImageResource(R.drawable.dice_6)
        }

        //A random number is generated and the dice on the RIGHT are changed according to this number.
        val diceRollRight = dice.roll()
        val diceImageRight: ImageView = findViewById(R.id.imageViewRight)
        when (diceRollRight) {
            1 -> diceImageRight.setImageResource(R.drawable.dice_1)
            2 -> diceImageRight.setImageResource(R.drawable.dice_2)
            3 -> diceImageRight.setImageResource(R.drawable.dice_3)
            4 -> diceImageRight.setImageResource(R.drawable.dice_4)
            5 -> diceImageRight.setImageResource(R.drawable.dice_5)
            6 -> diceImageRight.setImageResource(R.drawable.dice_6)
        }
    }
}

/**
 * Creates Dice class and roll function.
 */
class Dice(private val numSides: Int) {
    fun roll(): Int {
        return (1..numSides).random();
    }
}